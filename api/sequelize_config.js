const Sequelize = require('sequelize');
const Config = require('./config');
const Op = Sequelize.Op;

// docs http://docs.sequelizejs.com/class/lib/query-interface.js~QueryInterface.html
// http://sequelize.readthedocs.io/en/1.7.0/docs/migrations/

const sequelize = new Sequelize(Config.DB_NAME, Config.DB_USERNAME, Config.DB_PASSWORD, {
	host: Config.DB_HOST,
  	dialect: 'mysql',
  	port: Config.DB_PORT,
  	operatorsAliases: {
      $and: Op.and,
      $or: Op.or,
      $eq: Op.eq,
      $gt: Op.gt,
      $lt: Op.lt,
      $lte: Op.lte,
      $like: Op.like
    }
});

module.exports = sequelize;