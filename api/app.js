const Express = require('express');
const CookieParser = require('cookie-parser');
const Path = require('path');
/* -------------------------------------------------- */
const Authenticate = require('./auth/authenticate');
const Utils = require('./utils');
const ModelsActions = require('./models/actions');

const GETRoutes = require('./routes').get;
const POSTRoutes = require('./routes').post;

const Config = require('./config');
/* -------------------------------------------------- */
const BodyParser = require('body-parser');
const Favicon = require('serve-favicon');
const Helmet = require('helmet');

const Compression = require('compression');

const App = Express();
// tell the app to look for static files in these directories
App.use(Express.static(Path.resolve(__dirname, '..',  'static'), {index: false}));
App.use(Express.static(Path.resolve(__dirname, '..', 'build'), {index: false}));
App.use(Favicon(Path.resolve(__dirname, '..', 'static', 'favicon.ico')));


App.use(Compression());

App.use(Helmet());
// parse cookies
App.use(CookieParser());

App.use(BodyParser.json());

App.get('/res/users/list', ModelsActions.getUsersList);

App.get('*', (req, res) => {
    // TODO update cookie expire on each request
    Utils.isLoggedIn(req.cookies, (isLoggedIn) => {
        if (isLoggedIn) {
            if (Utils.isPublicRoute(req.url) || !Utils.isAllowedRoute(req.url)) {
                res.redirect( GETRoutes.authorizedRoute );
            } else {
                res.sendFile(Path.resolve(__dirname, '..', 'static', 'index.html'));
            }
        } else {
            if (Utils.isPublicRoute(req.url)) {
                res.sendFile(Path.resolve(__dirname, '..', 'static', 'index.html'));
            } else {
                res.redirect( GETRoutes.notAuthorizedRoute );
            }
        }
    });

});

App.route( POSTRoutes.login ).post(Authenticate.logIn);
App.route( POSTRoutes.logout ).post(Authenticate.logOut);
App.route( POSTRoutes.register ).post(Authenticate.register);
App.route( POSTRoutes.passwordReset ).post(ModelsActions.passwordReset);

module.exports = App;