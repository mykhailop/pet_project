const BCrypt = require('bcrypt');
const User = require('../models/user');
const Utils = require('../utils');

const updateModel = (fields, res, model) => {
    return model.update(fields, { fields: Object.keys(fields) })
        .then(user => {
            return res.status(200).json({
                message: {
                    type: 'success',
                    text: 'api.update.completed'
                },
                orm_errors: []
            });
        })
        .catch(error => {
            return res.status(400).json({
                message: {
                    type: 'danger',
                    text: 'api.update.failed'
                },
                orm_errors: error.errors
            });
        });
};

const updateUser = (req, res, fields = []) => {
    const userFields = {};

    fields.forEach((field) => {
        userFields[field] = req.body[field];
    });

    if (Utils.isEmptyRequest(Object.keys(userFields), userFields)) {
        return res.status(400).json({
            message: {
                type: 'danger',
                text: 'api.validation.invalid_login_or_email'
            },
            orm_errors: []
        });
    }

    // TODO Validate email and password for some SQL or code
    // TODO Check not by email but by USERID
    User.findOne({
        where: { email: req.body.email }
    }).then(user => {
        if (!user) {
            return res.status(401).json({
                message: {
                    type: 'danger',
                    text: 'api.validation.invalid_login_or_email'
                },
                orm_errors: []
            });
        }        

        if (userFields.password) {
            // create new hash for new password first
            BCrypt.hash(userFields.password, 10, (err, hash) => {
                userFields.password = hash;

                updateModel(userFields, res, user);
                
            });
        } else {
            updateModel(userFields, res, user);
        }
        
    }).catch(error => {
        return res.status(401).json({
            message: {
                type: 'danger',
                text: 'api.validation.server_error'
            },
            orm_errors: []
        });
    });    
};

const passwordReset = (req, res) => updateUser(req, res, [ 'password' ]);

const getUsersList = (req, res) => {
    Utils.isLoggedIn(req.cookies, (isLoggedIn) => {
        if (!isLoggedIn) {
            return res.status(403).json({});
        }

        return User.findAll({ attributes: { exclude: [ 'password', 'accessTo' ] } })
            .then(users => {
                return res.status(200).json({
                    data: users,
                    message: {
                        type: 'success',
                        text: 'api.get.completed',
                    },
                });
            });
    });
    
};

module.exports = {
    updateModel,
    updateUser,
    passwordReset,
    getUsersList,
};