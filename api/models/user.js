const Sequelize = require('sequelize');
const sequelize = require('../sequelize_config');

const User = sequelize.define('users', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    email: {
        type: Sequelize.STRING,
        defaultValue: '', // workaround for sequlize bug
        unique: { msg: 'This email is already exists' },
        validate: {
            notEmpty: { msg: 'The email cannot be empty' },
            isEmail:  { msg: 'Please provide correct email' },
        },
    },
    password: {
        type: Sequelize.STRING,
        defaultValue: '', // workaround for sequlize bug
        validate: {
            notEmpty: { msg: 'The password cannot be empty' },
        },
    },
    name: {
        type: Sequelize.STRING,
        defaultValue: '', // workaround for sequlize bug
        validate: {
            notEmpty: {msg: 'The name cannot be empty' },
        },
    },
    surname:   {
        type: Sequelize.STRING,
        allowNull: false,
    },
    accessTo: {
        type: Sequelize.STRING, // JSON with array of projects name
    },
    // ADDED BY MIGRATIONS
    // isActive: {
    //     type: Sequelize.BOOLEAN,
    //     defaultValue: '1'
    // },
    // lastAccess: {
    //     type: Sequelize.STRING
    // }
}, {
    charset: 'utf8',
    collate: 'utf8_unicode_ci',
});

User.sync();

module.exports = User;