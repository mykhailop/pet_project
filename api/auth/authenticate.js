const BCrypt = require('bcrypt');
const JWT = require('jsonwebtoken');

const User = require('../models/user');
const isEmptyRequest = require('../utils').isEmptyRequest;
const GETRoutes = require('../routes').get;
const Config = require('../config');

// response structure: {
//     message: {
//          type: 'success',
//          text: 'Register completed'
//     },
//     orm_errors: [{}, {}]
// }

/* REGISTER */
const register = (req, res) => {
    const userFields = {
        email: req.body.email,
        password: req.body.password,
        name: req.body.name,
        surname: req.body.surname,
    };

    if (isEmptyRequest(Object.keys(userFields), userFields)) {
        return res.status(400).json({
            message: {
                type: 'danger',
                text: 'api.validation.empty_login_or_email'
            },
            orm_errors: [],
        });
    }

    // create user with all fields
    BCrypt.hash(userFields.password, 10, (err, hash) => {
        userFields.password = hash;

        User.create(userFields)
            .then(user => {
                return res.status(200).json({
                    message: {
                        type: 'success',
                        text: 'api.register.completed',
                    },
                    orm_errors: [],
                });
            })
            .catch(error => {
                return res.status(400).json({
                    message: {
                        type: 'danger',
                        text: 'api.register.failed',
                    },
                    orm_errors: error.errors,
                    error: error,
                });
            });
    });
};

/* LOGIN */
const logIn = (req, res) => {
    const userFields = { email: req.body.email, password: req.body.password };

    if (isEmptyRequest(Object.keys(userFields), userFields)) {
        return res.status(400).json({
            message: {
                type: 'danger',
                text: 'api.validation.invalid_login_or_email',
            },
            orm_errors: [],
        });
    }

    // TODO Validate email and password for some SQL or code

    User.findOne({
        where: { email: req.body.email }
    }).then(user => {
        if (!user) {
            return res.status(401).json({
                message: {
                    type: 'danger',
                    text: 'api.validation.invalid_login_or_email'
                },
                orm_errors: [],
            });
        }

        // get hash from db
        BCrypt.compare(req.body.password, user.password, (err, isCorrect) => {
            // password is correct
            if (isCorrect === true) {

                const date = new Date();

                date.setMinutes(date.getMinutes() + Config.expirationNumber);

                const jwtParams = {
                    id: user.id,
                    surname: user.surname,
                    logged_in: new Date().toDateString(),
                };

                JWT.sign(jwtParams, Config.SECRET, {
                    expiresIn: Config.expirationNumber + "m", // expires in 3 hours
                    // algorithm: 'RS256' uncomment in case of https and cert file
                }, (err, token) => {
                    if (!token) {
                        return;
                    }

                    // add on production {secure: true, domain: 'our_domain', httpOnly: true}
                    res.cookie(Config.tokenKey, token, { expires: date });

                    const user_data = JSON.stringify({
                        userid: user.id,
                        name: user.name,
                        surname: user.surname
                    });

                    res.cookie('user_data', user_data, { expires: date });

                    return res.status(200).json({
                        message: {
                            type: 'success',
                            text: 'Login success'
                        },
                        redirect: GETRoutes.authorizedRoute,
                        orm_errors: [],
                    });
                });

            } else {
                // password is incorrect
                return res.status(401).json({
                    message: {
                        type: 'danger',
                        text: 'api.validation.invalid_login_or_email',
                    },
                    orm_errors: [],
                });
            }
        });
    }).catch(error => {
        return res.status(401).json({
            message: {
                type: 'danger',
                text: 'api.validation.server_error',
            },
            orm_errors: [],
            error: error,
        });
    });    
};

/* LOGOUT */
const logOut = function(req, res) {
    res.clearCookie(Config.tokenKey);
    res.status(200).json({ redirect: GETRoutes.notAuthorizedRoute });
};

module.exports = {
    logIn,
    logOut,
    register
};