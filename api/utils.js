const _ = require('underscore');
const GETRoutes = require('./routes').get;
const AllowedRoutes = require('./routes').allowed;
const Config = require('./config');
const JWT = require('jsonwebtoken');

const isLoggedIn = (cookies, callback) => {
	if (!_.isFunction(callback)) {
		console.error('isLoggedIn::lack of callback');
		return;
	}

	if (Boolean(cookies[Config.tokenKey]) === false) {
		return callback(false);
	}

	JWT.verify(cookies[Config.tokenKey], Config.SECRET, (err, decoded) => {
	  	callback(Boolean(decoded));
	});
};

const isPublicRoute = (url) => GETRoutes.public.includes(url);

const isAllowedRoute = (url) => AllowedRoutes.includes(url);

const isEmptyRequest = (reqFields = [], body = {}) => {
    return Boolean(reqFields.find((field) => !body[field] && body[field] !== 0));
};

module.exports = {
  	isPublicRoute,
  	isLoggedIn,
  	isEmptyRequest,
  	isAllowedRoute
};