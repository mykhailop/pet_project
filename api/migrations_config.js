const Config = require('./config');

module.exports = {
    development: {
        username: Config.DB_USERNAME,
        password: Config.DB_PASSWORD,
        database: Config.DB_NAME,
        host: Config.DB_HOST,
        port: Config.DB_PORT,
        dialect: 'mysql'
    }
};
