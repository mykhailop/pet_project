const _ = require('underscore');

const rootRoute = '';

const getRouteForGet = (route) => {
    if (Array.isArray(route)) {
        return route.map((element) => `${rootRoute}${element}`);
    }

    return rootRoute + route;
};

const getRouteForPost = (route) => route;

const get = {
    public: [ '/register', '/login', '/reset/password' ],
    notAuthorizedRoute: '/login',
    authorizedRoute: '/',
    logout: '/logout',
    register: '/register',
    passwordReset: '/reset/password',
    // modules
    usersList: '/users',
};

const post = {
    logout: '/logout',
    register: '/register',
    login: '/login',
    passwordReset: '/reset/password',
};

const getRoutes = _.mapObject(get, (value) => getRouteForGet(value));
const postRoutes = _.mapObject(post, (value) => getRouteForPost(value));

const allowedRoutes = _.uniq(_.flatten(_.values(postRoutes).concat(_.values(getRoutes))));

module.exports = {
    get: getRoutes,
    post: postRoutes,
    allowed: allowedRoutes,
};

