const config = require('../config');

const roles = ['USER', 'ADMIN'];
const PROJECTS = ['RST', 'LAW'];

const SECRET      = config.SECRET;
const DB_NAME     = config.DB_NAME;
const DB_USERNAME =	config.DB_USERNAME;
const DB_PASSWORD =	config.DB_PASSWORD;
const DB_PORT     =	config.DB_PORT;
const DB_HOST     =	config.DB_HOST;
const APP_PORT    =	config.APP_PORT;
const tokenKey    = config.tokenKey;

const expirationNumber = 360; // num of minutes to expire cookie and jwt

module.exports = {
  	roles,
  	expirationNumber,
  	SECRET,
  	DB_NAME,
  	DB_USERNAME,
  	DB_PASSWORD,
  	DB_PORT,
  	DB_HOST,
  	APP_PORT,
  	tokenKey,
};