'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        const migr1 = queryInterface.addColumn('users', 'isActive', {
            type: Sequelize.BOOLEAN,
            defaultValue: '1'
        });

        const migr2 = queryInterface.addColumn('users', 'lastAccess', {
            type: Sequelize.STRING
        });

        return Promise.all([migr1, migr2]);
    }
};
