const HOME_PATH = process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
const config = require(HOME_PATH + '/config.js');

module.exports = config;
