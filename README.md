# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1. Create your own configuration file in HOME directory with structure like:

    module.exports = {
        SECRET: "your db secret",
        DB_USERNAME: "username",
        DB_PASSWORD: "password",
        DB_NAME: "db name",
        DB_HOST: "localhost",
        DB_PORT: "db port, e.g. 3306",
        APP_PORT: "app port, e.g. 3000",
        tokenKey: "token_dev | token_prod",
    };

2. Run npm install
3. Run in one tab `npm run start` - starts a server in localhost
4. Run in another tab `npm run build` - starting the webpack

### Full list of command you can find in package.json ###
