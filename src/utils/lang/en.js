const lang = {
    // COMMON
    'common.button.login': 'Login',
    'common.button.download': 'Download',
    'common.button.logout': 'Logout',


    // MODULES
    // login
    'modules.login.login_btn': 'Login',
    'modules.login.register_btn': 'Sign in',
    'modules.login.reset_password_btn': 'Forgot password',
    'modules.login.fields.email': 'Email',
    'modules.login.fields.password': 'Password',
    'modules.login.placeholders.email': 'Enter an email',
    'modules.login.placeholders.password': 'Enter the password',
    // welcome page
    // titles
    'modules.welcome_page.woodpecker.title': 'Send cold mailing campaigns in Woodpecker',
    'modules.welcome_page.pipedrive.title': 'Manage leads and deals in Pipedrive',
    'modules.welcome_page.document.title': 'Manage prospects in Google Spreadsheet',
    // actions
    'modules.welcome_page.document.action': 'Go to Spreadsheet',
    'modules.welcome_page.woodpecker.action': 'Login to Woodpecker',
    'modules.welcome_page.pipedrive.action': 'Login to Pipedrive',
    // descriptions
    'modules.welcome_page.woodpecker.desc': 'In Woodpecker, you will be able to set up your automated mailing campaigns',
    'modules.welcome_page.pipedrive.desc': 'In Pipedrive, you will be able to manage your leads and contact your potential customers who expressed interest after the initial email. You will also be able to add potential leads from your own sources',
    'modules.welcome_page.document.desc': 'In your Google Spreadsheet, you can manage the database of your prospects and fill in their contact information',
    // document section
    'modules.welcome_page.list.document.1': '1. Complete the contact details of clients you want to approach (name, last name, email, position and the name of campaign where you want to include them) in the “baza danych” tab',
    'modules.welcome_page.list.document.2': '2. Copy the rows with selected prospects to the “baza danych (upload Woodpecker)” tab',
    'modules.welcome_page.list.document.3': '3. The copied contacts will be automatically uploaded to Woodpecker, where you will be able to create your email campaign',
    // woodpecker section
    'modules.welcome_page.list.woodpecker.1': '1. Create your campaign with initial email and subsequent follow-ups, using the necessary Snippets (Pan, Pani, etc.)',
    'modules.welcome_page.list.woodpecker.2': '2. Set up delivery times of your campaigns',
    'modules.welcome_page.list.woodpecker.3': '3. Add prospects to your campaigns (those which have been automatically uploaded via Google Spreadsheet)',
    'modules.welcome_page.list.woodpecker.4': '4. When somebody replies to an email, a deal is automatically created in Pipedrive',
    // pipedrive section
    'modules.welcome_page.list.pipedrive.1': '1. When somebody replies to an email sent from Woodpecker, a deal, a contact person and an organization is automatically created in Pipedrive',
    'modules.welcome_page.list.pipedrive.2': '2. Manage further sales process using Pipedrive',


    // COMPONENTS
    'components.footer.copyright': 'aplikacjedlabiznesu.pl All rights reserved',

    // VALIDATION
    'validation.not_empty': 'This field cannot be empty',
    'validation.invalid_length': "Field's length is invalid",
    'validation.invalid_email': 'The value should be an email',

    // API
    'api.validation.invalid_login_or_email': 'Invalid login or email',
    'api.validation.server_error': 'Something went wrong',
    'api.register.failed': 'Register failed. Please see messages below',
    'api.register.completed': 'New user was successfully created',
};

export default lang;