import langs from './langs';

const defaultLang = 'en';
const langMap = {
	'en-US': 'en',
	// 'pl': 'pl'
};

const getDefaultLang = () => {
	const userLang = window.navigator.language;

	return langMap[userLang] || defaultLang;
};

const translate = (token = '', opts = [], lang) => {
	const selectedLang = lang || getDefaultLang();
	const translations = langs[selectedLang];

	let translated = translations[token];

	if (!translated) {
		return token;
	}

	opts.forEach((param, index) => {
		translated = translated.replace(`$${index}`, param);
	});

 	return translated;
};

export { translate };

export default { translate };