const lang = {
	// MODULES
	'modules.login.login_btn': 'Zaloguj',
	'modules.login.fields.email': 'Email',
	'modules.login.fields.password': 'Hasło',
	'modules.login.placeholders.email': 'Wpisz swój email',
	'modules.login.placeholders.password': 'Wpisz hasło',

	// VALIDATION
	'validation.not_empty': 'To pole jest wymagane',
	'validation.invalid_length': 'Długość pola jest niepoprawna',
	'validation.invalid_email': 'Wartość ma być poprawnym emailem',

	// API
	'api.validation.invalid_login_or_email': 'Niepoprawny login lub hasło',
};

export default lang;