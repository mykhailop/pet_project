import { isString, isFunction, reduce } from 'underscore';

const notEmpty = (value, opts) => {
    if (!value && value !== 0) {
        return false;
    }

    if (isString(value) && !value.length) {
        return false;
    }

    return true;
};

const isEmail = (value) => {
    if (!isString(value)) {
        return false;
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(value);
};

const length = (value, opts = {}) => {
    if (!isString(value)) {
        return false;
    }

    if (opts.min > opts.max) {
        return false;
    }

    return !(value.length < opts.min || value.length > opts.max);
};

const globalConditions = {
    notEmpty,
    length,
    isEmail,
};

const check = (value = null, conditions = []) => {
    return reduce(conditions, (memo, condition) => {
        if (!isFunction(globalConditions[condition.name])) {
            return memo;
        }

        const isValid =  globalConditions[condition.name](value, condition.opts);

        if (!isValid) {
            memo.push({
                name: condition.name,
                validationMsg: condition.msg,
            });
        }

        return memo;
    }, []);
};

export {
    notEmpty,
    length,
    isEmail,
};

export default { check };