import { isString } from 'underscore';

const redirectTo = (path = '/') => {
	window.location.pathname = path;
};

export { redirectTo };

const getFromCookie = (name = '') => {
	if (!isString(name) || !name.length) {
		return '';
	}

  	const value = `; ${decodeURIComponent(document.cookie)}`;

  	const parts = value.split(`; ${name}=`);

  	return parts.length === 2 ? parts.pop().split(';').shift() : '';
};

export { getFromCookie };

const getUserData = () => {
	const unparsedUserData = getFromCookie('user_data');

	let user_data = '';

	try {
		user_data = JSON.parse(unparsedUserData);
	} catch(e) {
		console.warn(e);
	}

	return user_data;
};

export { getUserData };

const logout = async () => {
	const fetchParams = {
	    method: 'POST',
	    headers: { 'Content-Type': 'application/json' },
	};

    const fetchLogout = await fetch('/logout', fetchParams);
    const { redirect } = await fetchLogout.json();

    redirectTo(redirect);
};

export { logout };

export default { getFromCookie, logout, getUserData, redirectTo };