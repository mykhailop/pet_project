import React, { Component } from 'react';
import { observer } from 'mobx-react';
import List from 'components/list/list';
import Loader from 'components/loader/loader';

import UsersListStore from './users_list_store';

@observer
class UsersList extends Component {
    constructor(props) {
        super(props);

        this.store = new UsersListStore();
    }
    render() {
        if (!this.store || !this.store.resolved) {
            return <Loader />;
        }

        return (
            <div className="usersList">
                <List columns={this.store.columns} body={this.store.body} />
            </div>
        );
    }
}

export default UsersList;
