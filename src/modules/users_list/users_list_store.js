import { keys, first } from 'underscore';
import { observable } from 'mobx';

class UsersListStore {
    @observable resolved = false;
    @observable responseMessage = false;
    @observable columns = [];
    @observable body = [];

    constructor() {
        this.fetchList();
    }

    fetchList = async () => {
        const usersList = await fetch('/res/users/list');
        const json = await usersList.json();
        
        if (!Array.isArray(json.data) || !json.data.length) {
            return;
        }

        this.body.replace(json.data);
        this.columns.replace(keys(first(json.data)));

        this.resolved = true;
    }

}

export default UsersListStore;
