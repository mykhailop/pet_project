import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import WelcomePage from 'components/welcome_page/welcome_page';
import UsersList from 'modules/users_list/users_list';

import { get as GETRoutes } from 'api/routes';

function Content() {
    return (
        <div className="contentContainer">
            <Router>
                <Switch>
                    <Route exact path={GETRoutes.authorizedRoute} component={WelcomePage} />
                    <Route exact path={GETRoutes.usersList} component={UsersList} />
                </Switch>
            </Router>
        </div>
    );
}

export default Content;
