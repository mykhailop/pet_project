import { first, isEmpty, pick } from 'underscore';
import { observable, action, computed } from 'mobx';
import Validation from 'utils/validation';
import { redirectTo } from 'utils/utils';

class AuthStore {

    @observable resolved = true;
    @observable responseMessage = false;
    @observable responseType = false;
    @observable isActionBtnDisabled = false;

    fieldSchema = [];
    conditions = {};

    @computed get isFormValid() {
        return !Array.from(this.isValid.values()).includes(false);
    }

	constructor({ values = {}, conditions, fieldSchema }) {
        this.fieldSchema = fieldSchema;
        this.conditions = conditions;

	    this.modelMap = observable.map({});
        this.isValid = observable.map({});
        this.validationMsg = observable.map({});

        this.fieldSchema.forEach((field) => {
            this.modelMap.set(field.name, values[field] || '');
        });

	}

    validate(fields) {
        // validate one entry or whole form
        const fieldsToCheck = fields || Array.from(this.modelMap.keys());

        fieldsToCheck.forEach((key) => {
            const result = Validation.check(this.modelMap.get(key), this.conditions[key]);
                     
            // get just first error
            // we don't show all errors at the same moment
            const error = first(result);

            // in case of no error - return
            if (!error) {
                this.clearValidationForField(key);
                return;
            }

            this.setErrors({ path: key, message: error.validationMsg });
        });
    }

    @action clearValidation = () => {
        this.isValid.clear();
        this.validationMsg.clear();
    }

    @action clearValidationForField = (field) => {
        this.isValid.set(field, true);
        this.validationMsg.set(field, '');
    }

    @action setMessages = ({ text, type }) => {
        this.responseMessage = text;
        this.responseType = type;
    }

    @action setErrors = ({ path, message }) => {
        this.isValid.set(path, false);
        this.validationMsg.set(path, message);
    }

    @action clearModelMap = () => {
        Array.from(this.modelMap.keys()).forEach((key) => {
            this.modelMap.set(key, '');
        });
    }

	fetchData = async ({ url, fields }) => {
        if (this.isActionBtnDisabled || !url || !Array.isArray(fields)) {
            return;
        }

        this.setMessages({ text: false, type: false });
        this.validate();
        
        // when at least one field is invalid - dont trigger action
        if (!this.isFormValid) {
            return;
        }

        const fetchParams = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(pick(this.modelMap.toJSON(), ...fields)),
        };

        this.isActionBtnDisabled = true;

        const fetchData = await fetch(url, fetchParams);
        const response = await fetchData.json();

        this.clearValidation();
        this.setMessages(response.message);

        return { status: fetchData.status, ...response };        
    }

    defaultAction = async ({ url, fields }) => {
        const response = await this.fetchData({ url, fields });

        if (!response) {
            return;
        }

        const { orm_errors, status } = response;

        if (status === 200) {
            this.clearModelMap();
        } else {
            if (Array.isArray(orm_errors) && orm_errors.length) {
                orm_errors.forEach((error) => this.setErrors(error));
            }
        }

        this.isActionBtnDisabled = false;
    }

    registerAction = () => {
        this.defaultAction({
            url: '/register',
            fields: [ 'email', 'password', 'name', 'surname' ],
        });
    }

    resetPasswordAction = () => {
        this.defaultAction({
            url: '/reset/password',
            fields: [ 'email', 'password' ],
        });
    }

    loginAction = async () => {
        const response = await this.fetchData({
            url: '/login',
            fields: [ 'email', 'password' ],
        });

        if (!response) {
            return;
        }

        const { status, redirect } = response;

        if (status === 200) {
            redirectTo(redirect);
            return;
        }

        this.clearModelMap();

        this.isActionBtnDisabled = false;
    }

}

export default AuthStore;
