const fieldSchema = [ {
    name: 'email',
    type: 'text',
    placeholder: 'Enter email',
    label: 'Email address',
    addonIcon: 'fa fa-envelope',
}, {
    name: 'password',
    type: 'password',
    placeholder: 'Enter password',
    label: 'Password',
    addonIcon: 'fa fa-lock',
}, {
    name: 'name',
    type: 'text',
    placeholder: 'Enter first name',
    label: 'Name',
    addonIcon: 'fa fa-user',
}, {
    name: 'surname',
    type: 'text',
    placeholder: 'Enter your second name',
    label: 'Surname',
    addonIcon: 'fa fa-user-o',
} ];

export default fieldSchema;