export default {
	// EMAIL
	email: [ {
		name: 'notEmpty',
		msg: 'Email cannot be empty',
	}, {
		name: 'length',
		opts: { min: 3, max: 100 },
		msg: 'Email length is invalid',
	}, {
		name: 'isEmail',
		msg: 'The value should be an email',
	} ],

	// PASSWORD
	password: [ {
		name: 'notEmpty',
		msg: 'Password cannot be empty',
	}, {
		name: 'length',
		opts: { min: 6, max: 40 },
		msg: 'Password length is invalid',
	} ],

	// NAME
	name: [ {
		name: 'notEmpty',
		msg: 'Name cannot be empty',
	}, {
		name: 'length',
		opts: { min: 1, max: 100 },
		msg: 'Name length is invalid',
	} ],

	// SURNAME
	surname: [ {
		name: 'notEmpty',
		msg: 'Surname cannot be empty',
	}, {
		name: 'length',
		opts: { min: 1, max: 100 },
		msg: 'Surname length is invalid',
	} ],
};