import React, { PropTypes, Component } from 'react';
import { observer } from 'mobx-react';
import FormFieldInput from 'components/form_fields/form_field_input';
import TranslatedAlert from 'components/translated_alert/translated_alert';
import Button from 'components/button/button';

import AuthStore from 'modules/common/auth_store';
import conditions from 'modules/password_reset/conditions';
import fieldSchema from 'modules/password_reset/field_schema';

const defaultProps = {
    validateOnChange: true
};

@observer
class PasswordReset extends Component {

    constructor(props) {
        super(props);

        const { values } = props;

        this.store = new AuthStore({ values, conditions, fieldSchema });     
    }

    render() {
        return (
            <div className="passwordResetForm">
                <TranslatedAlert store={this.store} />

                { this.store.fieldSchema.map((field) => {
                    return (
                        <FormFieldInput
                            {...field}
                            key={field.name}
                            value={this.store.modelMap.get(field.name)}
                            isValid={this.store.isValid.get(field.name)}
                            validationMsg={this.store.validationMsg.get(field.name)}
                            onChange={(event) => {
                                this.store.modelMap.set(event.target.name, event.target.value);

                                if (this.props.validateOnChange) {
                                    this.store.validate([event.target.name]);
                                }
                            }}
                            
                        />
                    );
                }) }

                <div className="pull-right">
                    <Button
                        type="info"
                        token="Reset Password"
                        onClick={this.store.resetPasswordAction}
                        disabled={this.store.isActionBtnDisabled || !this.store.isFormValid}
                    />
                </div>

            </div>);
    }
}

PasswordReset.defaultProps = defaultProps;

export default PasswordReset;
