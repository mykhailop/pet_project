const fieldSchema = [ {
    name: 'email',
    type: 'text',
    placeholder: 'Enter email',
    label: 'Email address',
    addonIcon: 'fa fa-envelope',
}, {
    name: 'password',
    type: 'password',
    placeholder: 'Enter password',
    label: 'Password',
    addonIcon: 'fa fa-lock',
} ];

export default fieldSchema;
