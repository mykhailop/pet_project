export default {
    // EMAIL
    email: [ {
        name: 'notEmpty',
        msg: 'validation.not_empty',
    }, {
        name: 'length',
        opts: {min: 3, max: 100},
        msg: 'validation.invalid_length',
    }, {
        name: 'isEmail',
        msg: 'validation.invalid_email',
    } ],

    // PASSWORD
    password: [ {
        name: 'notEmpty',
        msg: 'validation.not_empty',
    }, {
        name: 'length',
        opts: {min: 4, max: 30},
        msg: 'validation.invalid_length',
    } ],
};