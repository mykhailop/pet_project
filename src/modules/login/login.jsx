import _ from 'underscore';
import React, { Component } from 'react';
import { observer } from 'mobx-react';
import Lang from 'utils/lang/lang';

import FormFieldInput from 'components/form_fields/form_field_input';
import TranslatedAlert from 'components/translated_alert/translated_alert';
import { Cell, Row } from 'components/grid';
import Link from 'components/link/link';
import Button from 'components/button/button';

import AuthStore from 'modules/common/auth_store';
import conditions from 'modules/login/conditions';
import fieldSchema from 'modules/login/field_schema';

const defaultProps = {
    validateOnChange: true
};


@observer
class Login extends Component {

    constructor(props) {
        super(props);

        const { values } = props;

        this.store = new AuthStore({ values, conditions, fieldSchema });     
    }

    render() {
        return (
            <div className="loginModule">
                <div className="loginFormContainer">
                    <Row className="alignCenter">
                        <Cell>
                            <img src="/img/logo.png" />
                        </Cell>
                    </Row>
                    <Row className="alignCenter">
                        <Cell className="fullWidth">
                            <TranslatedAlert store={this.store} />
                            
                            { this.store.fieldSchema.map((field) => {
                                return (
                                    <FormFieldInput
                                        {...field}
                                        key={field.name}
                                        value={this.store.modelMap.get(field.name)}
                                        isValid={this.store.isValid.get(field.name)}
                                        validationMsg={this.store.validationMsg.get(field.name)}
                                        onChange={(event) => {
                                            this.store.modelMap.set(event.target.name, event.target.value);
                                            
                                            if (this.props.validateOnChange) {
                                                this.store.validate([ event.target.name ]);
                                            }
                                        }}
                                    />
                                );
                            }) }

                            <Row>
                                <Cell className="takeRestOfSpace">
                                    <Link href="/reset/password" token="modules.login.reset_password_btn" />
                                </Cell>
                                <Cell>
                                    <Link href="/register" token="modules.login.register_btn" />
                                </Cell>
                                <Cell>
                                    <Button
                                        type="info"
                                        token="modules.login.login_btn"
                                        onClick={this.store.loginAction}
                                        disabled={this.store.isActionBtnDisabled || !this.store.isFormValid}
                                    />
                                </Cell>
                            </Row>
                        </Cell>
                    </Row>
                </div>
            </div>
        );
    }
}

Login.defaultProps = defaultProps;

export default Login;