const fieldSchema = [ {
    name: 'email',
    type: 'text',
    placeholder: 'modules.login.placeholders.email',
    label: 'modules.login.fields.email',
    addonIcon: 'fa fa-envelope',
}, {
    name: 'password',
    type: 'password',
    placeholder: 'modules.login.placeholders.password',
    label: 'modules.login.fields.password',
    addonIcon: 'fa fa-lock',
} ];

export default fieldSchema;