import React from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';
import { Jumbotron, Button } from 'react-bootstrap';


function WelcomePage() {
    return (
        <Jumbotron>
            <h1>Welcome to our simple app</h1>
            <p>
                Below you can find links to our modules and features. Enjoy.
            </p>
            <p>
                <Link to="/users">Users list</Link>
            </p>
            <p>
                <Link to="/register">Create new user</Link>
            </p>
        </Jumbotron>
    );
}

export default observer(WelcomePage);
