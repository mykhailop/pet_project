import React, { PropTypes, Component } from 'react';
import { observer } from 'mobx-react';
import Lang from 'utils/lang/lang';
import { getUserData, logout } from 'utils/utils';
import { Button, Navbar, MenuItem, Nav, NavDropdown } from 'react-bootstrap';

const { name, surname } = getUserData();

function Menu() {
    return (
        <Navbar>
            <Navbar.Header>
                <Navbar.Brand>
                    <a href="/">
                        <img width="120" height="80" src="/img/logo.png" />
                    </a>
                </Navbar.Brand>
            </Navbar.Header>
                
            <Nav pullRight>
                <NavDropdown
                    eventKey={3}
                    title={`${name} ${surname}`}
                    id="profile-dropdown"
                >
                    <MenuItem
                        eventKey={3.2}
                        onClick={logout}
                    >
                        { Lang.translate('common.button.logout') }
                    </MenuItem>
                </NavDropdown>
            </Nav>
        </Navbar>
    );
}

export default observer(Menu);
