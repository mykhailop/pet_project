import React from 'react';
import { observer } from 'mobx-react';
import { Link as ReactLink } from 'react-router-dom';
import Lang from 'utils/lang/lang';

function Link({ href, token }) {
    return (
        <ReactLink to={href}>
            { Lang.translate(token) }
        </ReactLink>
    );
}

export default observer(Link);
