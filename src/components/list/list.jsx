import { isEmpty } from 'underscore';
import Lang from 'utils/lang/lang'; 
import React from 'react';
import { Table } from 'react-bootstrap';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';

import ListHeader from './partials/list_header';
import ListBody from './partials/list_body';

// NOTICE structure of list tree
// columns: [ 'id', 'first_name', 'last_name' ],
// body: [ {
//     id: 1,
//     first_name: 'Mark',
//     last_name: 'Thornton',
// }, {
//     id: 2,
//     first_name: 'Jacob',
//     last_name: 'Otto',
// } ],
// customTdRenderer: {
//     id: (bodyEntry, column, index) => (
//         <td key={bodyEntry[column]}>
//             {'#' + bodyEntry[column]}
//         </td>
//     ),
// },

const defaultProps = {
    columns: [],
    body: [],
    customTdRenderer: {},
    listToken: 'content.list',
};

function List({ columns, body, listToken, customTdRenderer }) {
    if (isEmpty(toJS(columns)) || isEmpty(toJS(body))) {
        return null;
    }

    return (
        <Table striped bordered hover>
            <ListHeader columns={columns} listToken={listToken} />

            <ListBody columns={columns} body={body} customTdRenderer={customTdRenderer} />
        </Table>
    )
}

List.defaultProps = defaultProps;

export default observer(List);