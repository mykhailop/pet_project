import Lang from 'utils/lang/lang'; 
import React from 'react';

function ListHeader({ columns, listToken }) {
    return (
        <thead>
            <tr>
                { columns.map((column) => (
                    <th key={column}>{ Lang.translate(`${listToken}.${column}`) }</th>
                )) }
            </tr>
        </thead>
    )
}

export default ListHeader;
