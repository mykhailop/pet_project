import { keys, map, isFunction } from 'underscore';
import React from 'react';

function ListBody({ body, columns, customTdRenderer }) {
    return (
        <tbody>
            { map(body, (bodyEntry, index) => (
                <tr key={index}>
                    { map(columns, (column) => {
                        if (isFunction(customTdRenderer[column])) {
                            return customTdRenderer[column](bodyEntry, column, index);
                        }

                        return (
                            <td key={column + index}>
                                {bodyEntry[column]}
                            </td>
                        );
                    }) }
                </tr>
            )) }
        </tbody>
    )
}

export default ListBody;
