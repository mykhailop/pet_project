import React from 'react';
import { observer } from 'mobx-react';
import { Alert } from 'react-bootstrap';
import Lang from 'utils/lang/lang'; 

function TranslatedAlert({ store }) {
    if (!store.responseMessage) {
        return null;
    }

    return (
        <Alert
            bsStyle={store.responseType}
            onDismiss={() => { store.responseMessage = false; }}
        >
            { Lang.translate(store.responseMessage) }
        </Alert>
    );
}

export default observer(TranslatedAlert);
