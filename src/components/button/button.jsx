import { identity } from 'underscore';
import React from 'react';
import { observer } from 'mobx-react';
import { Button as ReactButton } from 'react-bootstrap';
import Lang from 'utils/lang/lang';

function Button({ type = 'info', token = '', onClick = identity, disabled = false }) {
    return (
        <ReactButton
            bsStyle={type}
            onClick={() => onClick()}
            disabled={disabled}
        >
            { Lang.translate(token) }
        </ReactButton>
    );
}

export default observer(Button);
