import React from 'react';
import { Navbar, Col, Nav, Row, Grid, NavItem } from 'react-bootstrap';
import Lang from 'utils/lang/lang'; 

function Footer() {

    return (
        <Navbar fixedBottom className="footer-component">                        
            <Nav>
                <NavItem>
            	    <Grid>
            	        <Row>
                            <Col xs={10} xsOffset={1} sm={6} smOffset={3} md={6} mdOffset={3} lg={5} lgOffset={3}>
                	           <i className="fa fa-copyright" />{' '}
                               {new Date().getFullYear()}{' '}
                               {Lang.translate('components.footer.copyright')}
                            </Col>
                        </Row>
                    </Grid>
                </NavItem>
            </Nav>
        </Navbar>
    );
}

export default Footer;