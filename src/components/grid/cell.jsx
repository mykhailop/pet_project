import React from 'react';
import { observer } from 'mobx-react';

function Cell({ children, className = '' }) {
	const cellClassName = `gridCell ${className}`;

    return (
        <div className={cellClassName}>
            { children }
        </div>
    );
}

export default observer(Cell);
