import React from 'react';
import { observer } from 'mobx-react';

function Row({ children, className = '' }) {
    const rowClassName = `gridRow ${className}`;

    return (
        <div className={rowClassName}>
            { children }
        </div>
    );
}

export default observer(Row);
