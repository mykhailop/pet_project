import _ from 'underscore';
import React from 'react';
import { observer } from 'mobx-react';
import Lang from 'utils/lang/lang';
import { FormGroup, InputGroup, FormControl, ControlLabel, Tooltip, OverlayTrigger } from 'react-bootstrap';

const getValidationState = (isValid) => {
    if (isValid === false) {
        return 'error';
    }

    if (isValid === true) {
        return 'success';
    }

    return null;
}


function FormFieldInput(props) {
    return (
        <FormGroup 
            validationState={getValidationState(props.isValid)}
        >
            <ControlLabel>{Lang.translate(props.label)}</ControlLabel>
            <InputGroup>
                <InputGroup.Addon><span className={props.addonIcon} /></InputGroup.Addon>
                <FormControl
                    type={props.type}
                    name={props.name}
                    value={props.value}
                    placeholder={Lang.translate(props.placeholder)}
                    onChange={props.onChange}
                />
            </InputGroup>
            { props.isValid === false ?
                <OverlayTrigger 
                    placement="top"
                    key={props.validationMsg.split(' ').join('_')} // to make rerender on each time
                    overlay={<Tooltip bsClass="validation-tooltip tooltip" id="tooltip">{Lang.translate(props.validationMsg)}</Tooltip>}
                    defaultOverlayShown
                >
                    <FormControl.Feedback>
                        <span className="fa fa-exclamation-circle" />
                    </FormControl.Feedback>
                </OverlayTrigger>
            : null }

            { props.isValid === true ?
                <FormControl.Feedback>
                   <span className="fa fa-check" />
                </FormControl.Feedback>
            : null }
        </FormGroup>
    );
}

export default observer(FormFieldInput);