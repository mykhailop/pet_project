import React from 'react';

import Menu from 'components/menu/menu';
import Content from './modules/content/content';
import Footer from 'components/footer/footer';

function App(props) {
    return (
        <div>
            <Menu {...props} />
            <Content {...props} />
            <Footer {...props} />
        </div>
    );
}

export default App;
