import React from 'react';
import ReactDOM from 'react-dom';
import { get as GETRoutes } from '../api/routes';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import App from './app';
import Login from 'modules/login/login';
import Register from 'modules/register/register';
import PasswordReset from 'modules/password_reset/password_reset';

ReactDOM.render(
  	<Router>
	  	<Switch>
			<Route exact path={GETRoutes.notAuthorizedRoute} component={Login} />
			<Route exact path={GETRoutes.register} component={Register} />
			<Route exact path={GETRoutes.passwordReset} component={PasswordReset} />
			<Route path={GETRoutes.authorizedRoute} component={App} />
		</Switch>
	</Router>,
  	document.getElementById('pet_project')
);