module.exports = {
    apps : [{
        name        : "production",
        script      : "./scripts/start_server.js",
        // error_file  : "err.log",
        // out_file    : "out.log",
        max_memory_restart: '150M',
        autorestart: true,
        nodeArgs: [
          "--use-strict"
        ],
        env: {
          "NODE_ENV": "production",
        }
    }, {
        name        : "development",
        script      : "./scripts/start_server.js",
        max_memory_restart: '150M',
        autorestart: true,
        nodeArgs: [
          "--use-strict"
        ],
        env: {
          "NODE_ENV": "production",
        }
    }],
    deploy : {
    // "production" is the environment name
        production : {
            user : "hatsen",
            host : ["207.154.219.174"],
            ref  : "origin/master",
            repo : "git@bitbucket.org:mykhailop/saasofsaas.git",
            path : "/var/www/html/production",
            "pre-deploy" : ["rm -rf node_modules"].join("; "),
            "post-deploy" : [
                "npm install --production",
                "rm -rf build",
                "npm run build:prod",
                "npm run migrations",
                "npm run start:prod"
            ].join("; "),
            "env"  : {
                "NODE_ENV": "production"
            }
        },
        development : {
            user : "hatsen",
            host : ["207.154.219.174"],
            ref  : "origin/develop",
            repo : "git@bitbucket.org:mykhailop/saasofsaas.git",
            path : "/var/www/html/development",
            "pre-deploy" : ["rm -rf node_modules"].join("; "),
            "post-deploy" : [
                "npm install --production",
                "rm -rf build",
                "npm run build:prod",
                "npm run migrations",
                "npm run start:dev"
            ].join("; "),
            "env"  : {
                "NODE_ENV": "production"
            }
        },
    },
}
