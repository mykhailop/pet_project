const path = require('path');
const webpack = require('webpack');
const entryPath = path.resolve(__dirname, 'src');
const buildPath = path.resolve(__dirname, 'build');

const MiniCSSExtractPlugin = require('mini-css-extract-plugin');

const commonConfig = {
    include: entryPath,
    loader: 'babel-loader',
    query: {
        presets: [ '@babel/preset-env', '@babel/preset-react' ],
        plugins: [
            [ '@babel/plugin-proposal-decorators', { legacy: true } ],
            '@babel/plugin-proposal-class-properties',
            '@babel/plugin-proposal-object-rest-spread',
            '@babel/plugin-transform-async-to-generator',
            '@babel/plugin-transform-runtime',
        ],
    },
};

module.exports = {
    mode: 'development',
    entry: {
        main: [entryPath + '/index.jsx', entryPath + '/styles/index.scss']
    },
    output: {
        path: buildPath,
        filename: '[name].js'
    },  
    plugins: [
        new MiniCSSExtractPlugin({
            filename: '[name].css',
            allChunks: true,
        })
    ],
    resolve: {
        alias: {
            components: path.resolve(__dirname, 'src/components'),
            modules: path.resolve(__dirname, 'src/modules'),
            utils: path.resolve(__dirname, 'src/utils'),
            api: path.resolve(__dirname, 'api')
        },
        extensions: ['.js', '.jsx'],
    },
    module: {
        // apply loaders to files that meet given conditions
        rules: [{
            test: /\.jsx?$/,
            ...commonConfig,
        }, {
            test: /\.js?$/,
            ...commonConfig,
        }, {
            test: /\.scss$/,
            include: entryPath + '/styles',
            use: [
                // { loader: 'style-loader' },
                { loader: MiniCSSExtractPlugin.loader },
                {
                    loader: 'css-loader',
                    // options: { modules: true },
                },
                'sass-loader',
            ]
        }, { 
            test: /\.(png|jpg)$/,
            include: path.join(__dirname, 'static/img'),
            loader: 'url-loader?limit=10000' 
        }],
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all',
                },
            },
        },
    },
};

// Version: webpack 3.8.1
// Time: 5011ms
// Asset      Size        Chunks               Chunk Names
// main.js    79.5 kB     0  [emitted]         main
// vendors.js 2.34 MB     1  [emitted]  [big]  vendors
// main.css   114 kB      0  [emitted]         main

// Version: webpack 4.42.1
// Time: 8456ms
// Built at: 04/21/2020 6:20:27 PM
// Asset      Size        Chunks             Chunk Names
// main.css   141 KiB     main    [emitted]  main
// main.js    115 KiB     main    [emitted]  main
// vendors.js 2.38 MiB    vendors [emitted]  vendors
