import { isPublicRoute, isLoggedIn, isEmptyRequest } from '../../api/utils';
import { get as GETRoutes } from '../../api/routes';
import assert from 'assert';


describe('Validation Module', () => {
    describe('isPublicRoute', () => {
        it('checking public route /login', () => {
            assert.equal(isPublicRoute('/login'), true);
        });
        it('checking public route /register', () => {
            assert.equal(isPublicRoute('/register'), true);
        });
        it('checking public route login', () => {
            assert.equal(isPublicRoute('login'), false);
        });
        it('checking public route from Config', () => {
            const route = Array.isArray(GETRoutes.public) && GETRoutes.public[0];

            assert.equal(isPublicRoute(route), true);
        });
    });

    describe('isEmptyRequest', () => {
        it('checking isEmptyRequest for non empty params', () => {
            const body = { email: 'test@gmail.com', password: '1234' };
            const fields = [ 'email', 'password' ];

            assert.equal(
                isEmptyRequest(fields, body),
                false,
            );
        });

        it('checking isEmptyRequest for empty params', () => {
            const body = { email: '', password: '1234' };
            const fields = [ 'email', 'password' ];

            assert.equal(
                isEmptyRequest(fields, body),
                true,
            );
        });

        it('checking isEmptyRequest for empty params', () => {
            const body = { email: 'test@gmail.com' };
            const fields = [ 'email', 'password' ];

            assert.equal(
                isEmptyRequest(fields, body),
                true,
            );
        });

        it('checking isEmptyRequest for non empty params', () => {
            const body = { email: 'test@gmail.com', clicks: 0 };
            const fields = [ 'email', 'clicks' ];

            assert.equal(
                isEmptyRequest(fields, body),
                false,
            );
        });
    });
});