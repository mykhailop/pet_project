import { notEmpty, isEmail, length } from '../../src/utils/validation';
import assert from 'assert';


describe('Validation Module', function() {
	describe('notEmpty function', function() {
	    it('checking if null is empty', function() {
	      	assert.equal(notEmpty(null), false);
	    });
	    it('checking if undefined is empty', function() {
	      	assert.equal(notEmpty(undefined), false);
	    });
	    it('checking if 0 is empty', function() {
	      	assert.equal(notEmpty(0), true);
	    });
	    it('checking if empty string is empty', function() {
	      	assert.equal(notEmpty(''), false);
	    });
	    it('checking if string is empty', function() {
	      	assert.equal(notEmpty('Mykhailo'), true);
	    });
	});

	describe('isEmail function', function() {
	    it('checking if asdf@sda.com is email', function() {
	      	assert.equal(isEmail('asdf@sda.com'), true);
	    });
	    it('checking if sadffs@ is email', function() {
	      	assert.equal(isEmail('sadffs@'), false);
	    });
	    it('checking if sadffs@sd is email', function() {
	      	assert.equal(isEmail('sadffs@sd'), false);
	    });
	    it('checking if null is email', function() {
	      	assert.equal(isEmail(null), false);
	    });
	    it('checking if misha.ppp@hotmail.com is email', function() {
	      	assert.equal(isEmail('misha.ppp@hotmail.com'), true);
	    });
	});

	describe('notEmpty function', function() {
	    it('checking length of ttt min: 1, max: 2', function() {
	      	assert.equal(length('ttt', {min: 1, max: 2}), false);
	    });
	    it('checking length of ttt min: 1, max: 4', function() {
	      	assert.equal(length('ttt', {min: 1, max: 4}), true);
	    });
	    it('checking length of ttt min: 5, max: ', function() {
	      	assert.equal(length('ttt', {min: 5, max: 2}), false);
	    });
	    it('checking length of ttt min: 5, max: 8', function() {
	      	assert.equal(length('ttt', {min: 5, max: 8}), false);
	    });
	});
});