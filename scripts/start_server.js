const App = require('../api/app');
const Config = require('../api/config');

// start the server
App.listen(Config.APP_PORT, () => {
  console.log('Server is running on http://localhost:' + Config.APP_PORT);
});