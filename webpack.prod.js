const path = require('path');
const webpack = require('webpack');
const entryPath = path.resolve(__dirname, 'src');
const buildPath = path.resolve(__dirname, 'build');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin');

// TODO migrate to webpack 4
module.exports = {
    //devtool: 'source-map', //'cheap-module-source-map'
    entry: {
        main: [entryPath + '/index.jsx', entryPath + '/styles/index.scss']
    },
    output: {
        path: buildPath,
        filename: '[name].js'
    },  
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendors',
            filename: 'vendors.js',
            minChunks (module) {
                return module.context && module.context.indexOf('node_modules') >= 0;
            }
        }),
        new webpack.optimize.LimitChunkCountPlugin({maxChunks: 2}),
        new ExtractTextPlugin({
            filename: '[name].css',
            allChunks: true
        }),
        new UglifyJsPlugin({
            uglifyOptions: {
                warnings: false,
                screw_ie8: true,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                if_return: true,
                join_vars: true
            }
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
    ],
    resolve: {
        alias: {
            components: path.resolve(__dirname, 'src/components'),
            modules: path.resolve(__dirname, 'src/modules'),
            utils: path.resolve(__dirname, 'src/utils')
        },
        extensions: ['.js', '.jsx']
    },
    module: {
        // apply loaders to files that meet given conditions
        rules: [{
            test: /\.jsx?$/,
            include: entryPath,
            // exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                presets: ['env', 'react'],
                plugins: ['transform-decorators-legacy', 'transform-object-rest-spread']
            }
        },{
            test: /\.js?$/,
            include: entryPath,
            loader: 'babel-loader',
            query: {
                presets: ['env', 'react'],
                plugins: ['transform-decorators-legacy', 'transform-object-rest-spread']
            }
        },{
            test: /\.scss$/,
            include: entryPath + '/styles',
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    { loader: 'css-loader', options: { minimize: true } },
                    'sass-loader'
                ]
            })
        }, { 
            test: /\.(png|jpg)$/,
            include: path.join(__dirname, 'static/img'),
            loader: 'url-loader?limit=10000' 
        }],
  }
};

// main.js     35.7 kB       0  [emitted]         main
// vendors.js  570 kB        1  [emitted]  [big]  vendors
// main.css    114 kB        0  [emitted]         main
